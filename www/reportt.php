<?php
	//require_once($_SERVER['DOCUMENT_ROOT'].'/delocalconf.php');
	require_once('delocalconf.php');
	require_once($DELIBDIR.'/php/page.php');
	require_once($DELIBDIR.'/php/entity.php');
	require_once($DELIBDIR.'/php/inst.php');
	decom_page_init();
	decom_page_set_title('Report');
	
	$con = '';
	$con .= '<pre>'; 
	
	$dobj = new DecomInstitute($_GET['did']);
	$dpptys = $dobj->getPublicPropertyNames();

	/* First row */
	foreach($dpptys as $p) {
		$con .= "$p\t";
	}

	$con .= "\n";

	/* Second row */
	foreach($dpptys as $p) {
		$con .= $dobj->getPropertyValue($p)."\t";
	}

	$con .= '</pre>';

	decom_page_set_content($con);
	decom_page_display();
?>

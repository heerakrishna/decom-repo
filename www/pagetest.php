<?php
	require_once('delocalconf.php');
	require_once($DELIBDIR.'/php/views/page.php');
	require_once($DELIBDIR.'/php/views/entity.php');
	require_once($DELIBDIR.'/php/menu.php');
	require_once($DELIBDIR.'/php/inst.php');
	
	decom_page_init();
	decom_page_set_title('Reports');
	$con =' ';
	//$menu = new DecomMenu();
	$smenu = new DecomMenu();
        $smenu->addItem(
        	new DecomMenuItem('Home','pagetest.php','home'));
       
        $footer = new DecomPageViewFooter();
	$footer->setCustomHtml('<p align=center>Copyright (C) 2019 Calicut university.</p>');
      
        $smenu->addItem(
                new DecomMenuItem('Annual Reports Of Departments','pagetest.php?page=annual','Annual Reports'));
       /* $smenu->addItem(
                new DecomMenuItem('Evaluative Reports','pagetest.php?page=evalrep','Evaluative Reports '));
        $smenu->addItem(
                new DecomMenuItem('SSR Reports','pagetest.php?page=ssrrep','SSR Reports'));*/  
        $smenu->addItem(
                new DecomMenuItem('Annual Quality Assuarance Reports','pagetest.php?page=aqar','AQA Reports'));
        $smenu->addItem(
                new DecomMenuItem('Research Reports','pagetest.php?page=research','Research Reports'));            
        $smenu->addItem(
                new DecomMenuItem('Library Audit','pagetest.php?page=library','Library Audit'));            
                
        
        //$con = ' ';
        decom_page_set_side_menu($smenu);	
        //decom_page_set_navbar($menu);
        $page = $_GET['page'];
        switch($page){
                
                case 'annual':include($_SERVER['DOCUMENT_ROOT'].'/../include/annual.php');
                     break;  
               /* case 'eval':include($_SERVER['DOCUMENT_ROOT'].'/../include/evalrep.php');
                     break;  
                      
                case 'ssr':include($_SERVER['DOCUMENT_ROOT'].'/../include/ssr.php');
                     break; */
                    
                case 'aqar':include($_SERVER['DOCUMENT_ROOT'].'/../include/aqar.php');
                     break;
                     
                case 'research':include($_SERVER['DOCUMENT_ROOT'].'/../include/research.php');
                     break;
                     
                case 'library':include($_SERVER['DOCUMENT_ROOT'].'/../include/library.php');
                     break;
               /* case 'add':include($_SERVER['DOCUMENT_ROOT'].'/../include/add.php');
                     break;*/
                     
                     default:"invalid case!!!";
                     }
        decom_page_set_footer($footer);            
        decom_page_set_content($con);
	decom_page_display();
?>
